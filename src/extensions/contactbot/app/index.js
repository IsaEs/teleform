const debug = require('debug')('extension:contactbot:app')
let router = require('express').Router()
// Telegram extension 
const bot  = require('../lib')
// Activate Telegram Bot
const TELEGRAM_TOKEN = process.env.TELEGRAM_TOKEN
debug(`Telegram Token ${TELEGRAM_TOKEN}`)
if (TELEGRAM_TOKEN != '') {
  debug('Created')
  // Telegram bot receiving updates at the route below
  router.post(`/bot${TELEGRAM_TOKEN}`, (req, res) => {
    bot.processUpdate(req.body)
    res.sendStatus(200)
  })
}
// All Bot Events
require('../events') 

module.exports = router